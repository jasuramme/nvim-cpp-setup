vim.api.nvim_create_user_command('Build',
function (opts)
    vim.cmd('! cd build && make');
end,
{bang = true})

vim.api.nvim_create_user_command('Configure',
function (opts)
    vim.cmd('! mkdir -p build && cd build && cmake ..');
end,
{bang = true})

