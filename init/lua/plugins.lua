return {
  "ethanholz/nvim-lastplace",
  "neovim/nvim-lspconfig",
  "folke/which-key.nvim",
  "mfussenegger/nvim-dap",
  "rcarriga/nvim-dap-ui",
  "nvim-lualine/lualine.nvim",
  "nvim-lua/plenary.nvim",
  "tanvirtin/vgit.nvim",
  "/nvim-telescope/telescope.nvim",
  "Mofiqul/vscode.nvim",
  "nvim-tree/nvim-web-devicons"
}
