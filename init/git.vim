lua require('vgit').setup()
menu git.&Diff :lua require('vgit').buffer_diff_preview()<CR>
menu git.Hunk\ Preview :lua require('vgit').buffer_hunk_preview()<CR>
menu git.&File\ History :lua require('vgit').buffer_history_preview()<CR>
menu git.&Blame\ line :lua require('vgit').buffer_blame_preview()<CR>
menu git.Blame\ file :lua require('vgit').buffer_gutter_blame_preview()<CR>
menu git.Diff\ Staged :lua require('vgit').buffer_diff_staged_preview()<CR>

command Git popup git
