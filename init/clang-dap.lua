local dap = require('dap')
dap.adapters.lldb = {
  type = 'executable',
  command = '/home/jasuramme/sandbox/llvm/llvm-project/build/bin/lldb-dap', -- adjust as needed, must be absolute path
  name = 'lldb'
}

local cpp_dap_cfg = {
    name = 'Launch',
    type = 'lldb',
    request = 'launch',
    program = vim.fn.getcwd() .. '/build/hello',
    cwd = '${workspaceFolder}',
    initCommands = {
      'b main'
    },
    sourceMap = { {'./', vim.fn.getcwd(),}, },
    stopOnEntry = false,
    args = {},
    --terminal = external,
    runInTerminal = false,
}

dap.configurations.cpp = {
    cpp_dap_cfg
}

require("dapui").setup()

vim.api.nvim_create_user_command('Debug',
function (opts)
    dap.run(cpp_dap_cfg);
    --dap.repl.open({height = 10});
    require('dapui').open();
end,
{bang = true})

vim.keymap.set('n', '<leader>dq', dap.step_out)
vim.keymap.set('n', '<leader>da', dap.step_over)
vim.keymap.set('n', '<leader>dz', dap.step_into)
vim.keymap.set('n', '<leader>ds', dap.pause)
vim.keymap.set('n', '<leader>dh', dap.close)
vim.keymap.set('n', '<leader>df', dap.toggle_breakpoint)
vim.keymap.set('n', '<leader>dc', dap.continue)
