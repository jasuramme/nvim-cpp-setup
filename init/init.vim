let mapleader = ","
set tabstop=4
set shiftwidth=4
set expandtab
set list
set lcs+=space:·
set number
set hlsearch
let g:config_dir="~/.config/nvim/"
nnoremap <leader>sv :source ~/.config/nvim/init.vim<CR>
execute "luafile" . g:config_dir . "init_lua.lua"

if filereadable(expand('./.nvim_workspace.vim'))
    exe 'source' './.nvim_workspace.vim'
endif

if filereadable(expand('./.nvim_workspace.lua'))
    exe 'luafile' './.nvim_workspace.lua'
endif
