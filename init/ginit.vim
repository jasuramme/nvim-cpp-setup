set background=dark
hi Normal guibg=#0d040d
hi Cursor guibg=#D0A0D0 guifg=white
call rpcnotify(1, 'Gui', 'Font', 'Hack Nerd Font 10')
" Enable Mouse
set mouse=a

" Set Editor Font
if exists(':GuiFont')
    " Use GuiFont! to ignore font errors
    GuiFont {font_name}:h{size}
endif

" Disable GUI Tabline
if exists(':GuiTabline')
    GuiTabline 0
endif

" Disable GUI Popupmenu
if exists(':GuiPopupmenu')
    GuiPopupmenu 0
endif

" Enable GUI ScrollBar
if exists(':GuiScrollBar')
    GuiScrollBar 1
endif

inoremap <S-Insert> <C-R>*
nnoremap <S-Insert> i<C-R>*
vnoremap <S-Insert> <C-R>*
cnoremap <S-Insert> <C-R>*
" Right Click Context Menu (Copy-Cut-Paste)
"nnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>
"inoremap <silent><RightMouse> <Esc>:call GuiShowContextMenu()<CR>
"xnoremap <silent><RightMouse> :call GuiShowContextMenu()<CR>gv
"snoremap <silent><RightMouse> <C-G>:call GuiShowContextMenu()<CR>gv

exe 'source' '~/.config/nvim/ginit.lua'
